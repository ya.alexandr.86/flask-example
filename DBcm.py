import mysql.connector


class UserDatabase():


    def __init__(self, config: dict) -> None: # инициализация
        self.configuration = config


    
    def __enter__(self) -> 'cursor': # настройки
        self.conn = mysql.connector.connect(**self.configuration)
        self.cursor = self.conn.cursor()
        return self.cursor


    def __exit__(self, exc_type, exc_value, exc_trace) -> None: #завершающие операции
        self.conn.commit()
        self.cursor.close()
        self.conn.close()
